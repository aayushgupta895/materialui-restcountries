import { useEffect, useState } from "react";

import "./App.css";
import { useMyContext } from "./MyContext";
import { Home } from "./pages/home/Home";
import { Route, Routes } from "react-router-dom";
import CountryDetails from "./pages/countryDetails/CountryDetails";
import Navbar from "./components/navbar/Navbar";

/* material */
import { Button, Paper, Typography } from "@mui/material";

function App() {
  const [countries, setCountries] = useState([]);
  const [subRegionsAvailable, setSubRegionsAvailable] = useState({});
  const [loader, setLoader] = useState(true);

  const [apiError, setApiError] = useState(false);
  const { darkMode } = useMyContext();

  useEffect(() => {
    const getData = async () => {
      try {
        let data = await fetch("https://restcountries.com/v3.1/all");
        data = await data.json();
        setCountries(data);
        setLoader(false);
        const subRegion = {};
        data.forEach((country) => {
          if (subRegion[country?.region]) {
            if (!subRegion?.[country.region].includes(country?.subregion))
              subRegion?.[country.region].push(country?.subregion);
          } else {
            subRegion[country.region] = [country.subregion];
          }
        });
        setSubRegionsAvailable(subRegion);
      } catch (error) {
        console.log(`some error occurred : `, error);
        setApiError(true);
      }
    };
    getData();
  }, []);

  return (
    <div className={` ${darkMode ? "dark" : "parent"}`}>
      {apiError ? (
        <Typography variant="h4" color='#000'>Service unavailable</Typography>
      ) : (
        <>
          <Navbar />
          <Routes>
            <Route
              path="/"
              element={
                <Home
                  apiError={apiError}
                  countries={countries}
                  subRegionsAvailable={subRegionsAvailable}
                  loader={loader}
                />
              }
            ></Route>
            <Route path="/country/:id" element={<CountryDetails />}></Route>
          </Routes>
        </>
      )}
    </div>
  );
}

export default App;
