import React from "react";
import App from "./App.jsx";
import "./index.css";
import { useMyContext } from "./MyContext";
import { ThemeProvider, createTheme, CssBaseline, Paper } from "@mui/material";

const darkTheme = createTheme({
  palette: {
    mode: "dark",
    text: {
      primary: "hsl(0, 0%, 100%)",
    },
    background: {
      default: "hsl(207, 26%, 17%)",
      paper: "hsl(209, 23%, 22%)",
    },
  },
  typography: {
    fontSize: 16,
    fontFamily: `"Nunito Sans", sans-serif`,
  },
  spacing: {
    xl : 16,
    lg : 6,
  },
});

const lightTheme = createTheme({
  palette: {
    mode: "light",
    text: {
      primary: "hsl(200, 15%, 8%)",
    },
    background: {
      default: "hsl(0, 0%, 98%)",
      paper: "hsl(0, 0%, 100%)",
    },
  },
  typography: {
    fontSize: 16,
    fontFamily: `"Nunito Sans", sans-serif`,
  },
  spacing: 16,
});

function CustomThemeProvider() {
  const { darkMode } = useMyContext();
  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  );
}

export default CustomThemeProvider;
