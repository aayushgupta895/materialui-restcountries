import React from "react";
import "./style.css";
import { useNavigate } from "react-router-dom";
import { Box, Card, CardMedia, Paper, Typography } from "@mui/material";

function CountryContainer(props) {
  const { country } = props;
  const navigate = useNavigate();
  return (
    <Paper
      sx={{
        borderRadius: "0.5rem",
        overflow: "hidden",
      }}
    >
      <Card
        onClick={() => navigate(`country/${country.cca2}`)}
      >
        <CardMedia
          image={country.flags.png}
          title={country.flags.alt}
          sx={{
            height: "13.25rem",
            width: "21.56rem",
          }}
        />
        <Box
          sx={{
            pl: 1.7,
            pt: 1.5,
            pb: 3,
          }}
        >
          <Box
            sx={{
              fontSize: "1.3rem",
              fontWeight: 800,
              mb: 0.8,
              maxWidth: "18rem",
            }}
          >
            {country.name.common}
          </Box>
          <Box>
            <Typography
              variant="span"
              sx={{
                fontWeight: 600,
                fontSize: "1.1rem",
              }}
            >
              Population
            </Typography>{" "}
            :{country.population}
          </Box>
          <Box>
            <Typography
              variant="span"
              sx={{
                fontWeight: 600,
                fontSize: "1.1rem",
              }}
            >
              Region
            </Typography>{" "}
            : {country.region}
          </Box>
          <Box>
            <Typography
              variant="span"
              sx={{
                fontWeight: 600,
                fontSize: "1.1rem",
              }}
            >
              Capital
            </Typography>{" "}
            : {country.capital}
          </Box>
        </Box>
      </Card>
    </Paper>
  );
}

export default CountryContainer;
