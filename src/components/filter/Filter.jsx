import React from "react";
import "./style.css";
import { Box,  InputBase, MenuItem, Paper, Select,  Divider } from "@mui/material";

import SearchIcon from "@mui/icons-material/Search";


function Filter(props) {
  const {
    setSearch,
    regionFilter,
    setRegionFilter,
    subRegionsAvailable,
    setSubRegion,
    subRegion,
    setSortBy,
    sortBy,
  } = props;
  return (
    <Box sx={{
      display : 'flex',
      alignItems : 'center',
      boxSizing : "border-box",
      px : 8,
      width : "100%",
      mt : 3.5,
      justifyContent : "space-between" 
    }}>
      <Paper
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center", // Center horizontally
          width: "35rem",
          height: "3.5rem",
        }}
      >
        <SearchIcon sx={{ color: "grey" }} />
        <InputBase
          sx={{
            width: "80%",
            marginLeft: "2rem",
          }}
          placeholder="Search for a country"
          onChange={(e) => setSearch(e.target.value.trim())}
        />
      </Paper>
      <Paper>
        <Box>
          <Select
            id="regions"
            name="continent"
            value={regionFilter}
            onChange={(e) => {
              setRegionFilter(e.target.value);
              setSubRegion("");
            }}
            sx={{
              width: "18rem",
              height: "3.9rem",
            }}
            displayEmpty
          >
            <MenuItem value="">Filter by Region</MenuItem>
            <Divider />
            <MenuItem value="Asia">asia</MenuItem>
            <MenuItem value="Africa">africa</MenuItem>
            <MenuItem value="Americas">america</MenuItem>
            <MenuItem value="Oceania">Australia</MenuItem>
            <MenuItem value="Europe">Europe</MenuItem>
          </Select>
        </Box>
      </Paper>
      <Paper>
        <Box>
          <Select
            id="subregions"
            name="subregion"
            value={subRegion}
            onChange={(e) => setSubRegion(e.target.value.trim())}
            sx={{
              width: "18rem",
              height: "3.9rem",
            }}
            displayEmpty
          >
            <MenuItem value=""> Filter by subregion</MenuItem>
            <Divider />
            {subRegionsAvailable[regionFilter] &&
              subRegionsAvailable[regionFilter].map((subrgn) => {
                return (
                  <MenuItem key={subrgn} value={`${subrgn}`}>
                    {subrgn}
                  </MenuItem>
                );
              })}
          </Select>
        </Box>
      </Paper>
      <Paper>
        <Box>
          <Select
            id="sort"
            name="sort"
            value={sortBy}
            onChange={(e) => setSortBy(e.target.value.trim())}
            sx={{
              width: "18rem",
              height: "3.9rem",
            }}
            displayEmpty
          >
            <MenuItem value="">sort by</MenuItem>
            <Divider />
            <MenuItem value="population-desc">
              population (high to low)
            </MenuItem>
            <MenuItem value="population-asc">population (low to high)</MenuItem>
            <MenuItem value="area-desc">Area (high to low)</MenuItem>
            <MenuItem value="area-asc">Area (low to high)</MenuItem>
          </Select>
        </Box>
      </Paper>
    </Box>
  );
}

export default Filter;
