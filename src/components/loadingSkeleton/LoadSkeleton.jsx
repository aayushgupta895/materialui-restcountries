import Skeleton from "react-loading-skeleton";
import "./style.css";
import { Box } from "@mui/material";

let LoadSkeleton = ({ loop }) => {
  return (
    <>
      {Array(loop)
        .fill(0)
        .map((_, i) => {
          return (
            <Box key={i} sx={{
              height: "387px",
              width: "344px",
            }}>
              <Skeleton height={"50%"} style={{ marginBottom: "0.5rem" }} />
              <Box sx={{
                p : 1
              }}>
                <Skeleton
                  count={4}
                  height={"1rem"}
                  width={"100%"}
                  style={{ marginBottom: "0.5rem" }}
                />
              </Box>
            </Box>
          );
        })}
    </>
  );
};
export default LoadSkeleton;
