import React from "react";
import "./style.css";
import { useMyContext } from "../../MyContext";
import { Box, Paper, Typography } from "@mui/material";

function Navbar() {
  function enableDarkMode() {
    setDarkMode(!darkMode);
  }
  const { darkMode, setDarkMode } = useMyContext();
  return (
    <Paper>
      <Box
        component="section"
        sx={{
          height: "11vh",
          width: "100%",
          display: "flex",
          alignItems: "center",
          px: "8rem",
          boxSizing: "border-box",
          boxShadow: 2,
        }}
        className={`${darkMode ? "nav-dark" : ""}`}
      >
        <Typography
          variant="h1"
          sx={{
            fontSize: "1.82rem",
            fontWeight: "800",
          }}
        >
          Where in the world?
        </Typography>
        <Box
          sx={{
            ml: "auto",
            display: "flex",
            alignItems: "center",
            gap: 1,
            cursor: "pointer",
          }}
          className="darkToggler"
        >
          <i className="fa-solid fa-moon solid"></i>
          <i className="fa-regular fa-moon regular"></i>
          <Typography
            sx={{
              fontSize: "1.35rem",
              fontWeight: "600",
            }}
            onClick={() => enableDarkMode()}
          >
            {darkMode ? `Light Mode` : `Dark Mode`}
          </Typography>
        </Box>
      </Box>
    </Paper>
  );
}

export default Navbar;
