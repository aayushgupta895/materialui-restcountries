import React from "react";
import ReactDOM from "react-dom/client";
import CustomThemeProvider from "./CustomThemeProvider.jsx";
import { BrowserRouter } from "react-router-dom";
import { MyProvider } from "./MyContext.jsx";



ReactDOM.createRoot(document.getElementById("root")).render(
  <>
    <BrowserRouter>
      <MyProvider>
        <CustomThemeProvider/>
      </MyProvider>
    </BrowserRouter>
  </>
);
