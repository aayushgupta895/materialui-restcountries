import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { GoArrowLeft } from "react-icons/go";
import "./style.css";
import { useMyContext } from "../../MyContext";

function CountryDetails() {
  const { id } = useParams();
  const [country, setCountry] = useState();
  const [detailLoader, setDetailLoader] = useState(true);
  const navigate = useNavigate();
  const { darkMode } = useMyContext();

  useEffect(() => {
    const getData = async () => {
      try {
        let data = await fetch("https://restcountries.com/v3.1/all");
        data = await data.json();
        data = data.find((cntry) => {
          return cntry.cca2 == id;
        });
        console.log(data);
        setCountry(data);
        setDetailLoader(false);
      } catch (error) {
        console.log(`some error occurred : `, error);
        setApiError(true);
      }
    };
    getData();
  }, []);

  return (
    <div className={`countryDetails ${darkMode ? "dark-mode" : ""}`}>
      <div className="back-btn" onClick={() => navigate(-1)}>
        <GoArrowLeft
          className={`left-arrow ${darkMode ? "dark-back-icon" : ""}`}
        />
        <button>Back</button>
      </div>
      <div className="detailContainer">
        <div className="country-flag">
          <img src={country?.flags.svg} alt={country?.flags.alt} />
        </div>
        <div className="detail-sec">
          <div className="detail-sec1">
            <div className="detail-part1">
              <div className="country-name-heading">
                {country?.name?.common}
              </div>
              <p>
                <span className="detail-fields">Native Name: </span>
                {country ? (
                  Object.values(country?.name?.nativeName)[0].common
                ) : (
                  <></>
                )}
              </p>
              <p>
                <span className="detail-fields">Population: </span>
                {country?.population}
              </p>
              <p>
                <span className="detail-fields">Region: </span>
                {country?.region}
              </p>
              <p>
                <span className="detail-fields">Sub Region: </span>
                {country?.subregion}
              </p>
              <p>
                <span className="detail-fields">Capital: </span>
                {country?.capital[0]}
              </p>
            </div>
            <div className="detail-part2">
              <p>
                <span className="detail-fields">Top Level Domain: </span>
                {country?.tld[0]}
              </p>
              <p>
                <span className="detail-fields">Currencies: </span>
                {country ? Object.values(country?.currencies)[0].name : <></>}
              </p>
              <p>
                <span className="detail-fields">Languages: </span>
                {country ? Object.values(country?.languages).join(", ") : <></>}
              </p>
            </div>
          </div>
          <div className="detail-sec2">
            <p>Border Countries :</p>
            <div className="border-country">
              {country?.borders ? (
                country?.borders.map((borderCountry) => {
                  return (
                    <button key={country?.name?.common}>{borderCountry}</button>
                  );
                })
              ) : (
                <>No border Country</>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CountryDetails;
