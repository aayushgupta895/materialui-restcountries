import { useState } from "react";
import "./style.css";

import Navbar from "../../components/navbar/Navbar";
import CountryContainer from "../../components/countryContainer/CountryContainer";
import { useMyContext } from "../../MyContext";
import Filter from "../../components/filter/Filter";
import LoadSkeleton from "../../components/loadingSkeleton/LoadSkeleton";
import "react-loading-skeleton/dist/skeleton.css";
import { Box, Typography } from "@mui/material";

export const Home = (props) => {
  const [regionFilter, setRegionFilter] = useState("");
  const [subRegion, setSubRegion] = useState("");
  const [search, setSearch] = useState("");
  const [sortBy, setSortBy] = useState("");

  const { apiError, countries, subRegionsAvailable, loader } = props;
  const { darkMode } = useMyContext();

  let filterCountry = [];

  if (!apiError)
    filterCountry = countries
      .filter(
        (country) =>
          (regionFilter == "" || country?.region == regionFilter) &&
          (country?.name?.common
            ?.toUpperCase()
            ?.includes(search.toUpperCase()) ||
            country?.name?.official
              ?.toUpperCase()
              ?.includes(search.toUpperCase())) &&
          (subRegion == "" || country?.subregion == subRegion)
      )
      .sort((a, b) => {
        if (sortBy === "population-desc") {
          return b?.population - a?.population;
        } else if (sortBy === "population-asc") {
          return a?.population - b?.population;
        } else if (sortBy === "area-desc") {
          return b?.area - a?.area;
        } else if (sortBy === "area-asc") {
          return a?.area - b?.area;
        }
      });

  return (
    <>
      <Filter
        setSearch={setSearch}
        setRegionFilter={setRegionFilter}
        regionFilter={regionFilter}
        subRegionsAvailable={subRegionsAvailable}
        subRegion={subRegion}
        setSubRegion={setSubRegion}
        sortBy={sortBy}
        setSortBy={setSortBy}
      />
      <Box
        sx={{
          width: "100vw",
          height: "fit-content",
          display: "flex",
          flexWrap: "wrap",
          boxSizing: "border-box",
          gap: {xl : "4.8vw", lg : "1.8vw"},
          px: 8,
          mt: 5,
          pb: 4,
          cursor: "pointer",
          overflow: "hidden",
        }}
      >
        {loader ? (
          <LoadSkeleton loop={8} />
        ) : filterCountry.length != 0 ? (
          filterCountry.map((country) => (
            <CountryContainer key={country.name.common} country={country} />
          ))
        ) : (
          <Typography sx={{
            textAlign : "center",
            fontSize :  "2rem"
          }}>No country available</Typography>
        )}
      </Box>
    </>
  );
};
